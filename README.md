# Penguin 

- **OS:** Arch Linux
- **WM:** [Sway](https://swaywm.org/)
- **Shell:** zsh
- **Colorscheme:** [Gruvbox](https://github.com/morhetz/gruvbox)
- **Notifications:** Mako
- **Terminal:** Alacritty
- **Browser:** Firefox + [Blurred Fox](https://github.com/manilarome/blurredfox)
- **Launcher:** [Sway Launcher](https://github.com/Biont/sway-launcher-desktop)
- **Spotify Client:** [Spicetify](https://github.com/khanhas/spicetify-cli)

<img src="https://gitlab.com/crayarc/penguin/-/raw/master/clean.png" width="800">
<img src="https://gitlab.com/crayarc/penguin/-/raw/master/spotify.png" width="800">
<img src="https://gitlab.com/crayarc/penguin/-/raw/master/ff.png" width="800">
<img src="https://gitlab.com/crayarc/penguin/-/raw/master/dirty.png" width="800">
<img src="https://gitlab.com/crayarc/penguin/-/raw/master/swaylaunch.png" width="800">
