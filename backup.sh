#!/bin/zsh
# backup files and push

# copy configs + dotfiles
cp ~/.config/sway/config ~/.Penguin/config
cp ~/.config/waybar/config ~/.Penguin/waybarconfig
cp ~/.alacritty.yml ~/.Penguin/alacritty.yml
cp ~/.mozilla/firefox/mkz6d0um.default-release/chrome/colors/solid.css ~/.Penguin/firefox.css

# git 
#git add .{vimrc,xinitrc,Xresources,zshrc,zprofile} polybarconfig i3config next.txt
#git commit -m "update"
#git push -u origin master

